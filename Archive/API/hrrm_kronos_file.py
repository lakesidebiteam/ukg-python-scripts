import pandas as pd
import os
# Create Output File

infile_name = "UKGColumnHeaders.csv"
outfile_name = "ProcessedAPIOutput.csv"
os_dl_path = ""

infile = os.path.join(os_dl_path,infile_name)
outfile = os.path.join(os_dl_path,outfile_name)

def load_header():
    df = pd.read_csv(infile, delimiter="|", header=0, converters={'udLocation' :str, 'Phone' :str, 'udSupervisor' :str, 'Zip' :str, 'HandicapDesc' :str, 'HandicapYN' :str, 
    'WOLocalCode' :str, 'udLocation' :str, 'WOTaxState' :str, 'HandicapYN' :str, 'StdUnempState' :str, 'StdInsState' :str, 'State' :str})
    return df

def load_default_values(df, i):
    df.at[i, 'HRCo'] = 1
    df.at[i, 'PRCo'] = 1
    df.at[i, 'Pager'] = ''
    df.at[i, 'W4CompleteYN'] = 'Y'
    df.at[i, 'NoRehireYN'] = 1
    df.at[i, 'MaidenName'] = ''
    df.at[i, 'SpouseName'] = ''
    df.at[i, 'PassPort'] = 'N'
    df.at[i, 'RelativesYN'] = 'N'
    df.at[i, 'HandicapYN'] = 'N'
    df.at[i, 'PhysicalYN'] = 'Y'
    df.at[i, 'PhysResults'] = ''
    df.at[i, 'LicNumber'] = ''
    df.at[i, 'LicType'] = ''
    df.at[i, 'HiringLocationID'] = 1
    df.at[i, 'DriveCoVehiclesYN'] = 'Y'
    df.at[i, 'I9Status'] = 'E'
    df.at[i, 'TrainingBudget'] = ''
    df.at[i, 'CafeteriaPlanBudget'] = ''
    df.at[i, 'HighSchool'] = ''
    df.at[i, 'HSGradDate'] = ''
    df.at[i, 'College1'] = ''
    df.at[i, 'College1BegDate'] = ''
    df.at[i, 'College1EndDate'] = ''
    df.at[i, 'College1Degree'] = ''
    df.at[i, 'College2'] = ''
    df.at[i, 'College2BegDate'] = ''
    df.at[i, 'College2EndDate'] = ''
    df.at[i, 'College2Degree'] = ''
    df.at[i, 'ApplicationDate'] = ''
    df.at[i, 'AvailableDate'] = ''
    df.at[i, 'LastContactDate'] = ''
    df.at[i, 'ExpectedSalary'] = ''
    df.at[i, 'Source'] = ''
    df.at[i, 'SourceCost'] = ''
    df.at[i, 'CurrentEmployer'] = ''
    df.at[i, 'CurrentTime'] = ''
    df.at[i, 'PrevEmployer'] = ''
    df.at[i, 'PrevTime'] = ''
    df.at[i, 'NoContactEmplYN']= 'N'
    df.at[i, 'Notes'] = ''
    df.at[i, 'ExistsInPR'] = 'Y'
    df.at[i, 'Suffix'] = ''
    df.at[i, 'LicClass'] = ''
    df.at[i, 'NonResAlienYN'] = 'N'
    df.at[i, 'Country'] = 'USA'
    df.at[i, 'LicCountry'] = ''
    df.at[i, 'OTOpt'] = ''
    df.at[i, 'OTSched'] = ''
    df.at[i, 'Shift'] = 1
    df.at[i, 'PTOAppvrGrp'] = ''
    df.at[i, 'HDAmount'] = ''
    df.at[i, 'F1Amt'] = ''
    df.at[i, 'LCFStock'] = ''
    df.at[i, 'LCPStock'] = ''
    df.at[i, 'WOTaxState'] = ''
    df.at[i, 'WOLocalCode'] = ''
    df.at[i, 'StatusCode'] = ''
    df.at[i, 'LookBackGroup'] = ''
    df.at[i, 'InitialMeasurementStartDate'] = ''
    df.at[i, 'InitialMeasurementEndDate'] = ''
    df.at[i, 'InitialAdministrativeStartDate'] = ''
    df.at[i, 'InitialAdministrativeEndDate'] = ''
    df.at[i, 'InitialStabilityStartDate'] = ''
    df.at[i, 'InitialStabilityEndDate'] = ''
    df.at[i, 'udDrugTestPool'] = ''
    df.at[i, 'udHandbookAcknowledgementDate'] = ''
    df.at[i, 'udTelecommutingExpiration'] = ''
    df.at[i, 'udVaccinationAttestation'] = ''
    return df

def LoadInitialDataFrame():
    df = pd.DataFrame(load_header)
    

def delete_test_data(df):
    df = df[df['object.last_name'].str.upper() != 'TEST']
    df = df[df['object.last_name'].str.upper() != 'UKG ACCOUNT']
    df = df[df['object.last_name'].str.upper() != 'ACCOUNT']
    df = df[df['object.last_name'].str.upper() != 'API']
    df = df[df['object.employee_id'] != 13341]
    return df

