import pyodbc
import time
import pandas as pd
from Repo.env_var import env

e = env()
def __return_connection_string():
    connection_type = e.environment
    dev_conn_str = ("Driver={SQL Server Native Client 11.0};"
                "Server=ERPTESTSRV;"
                "Database=Viewpoint;"
                "Trusted_Connection=yes;"
            )

    prod_conn_str = ("Driver={SQL Server Native Client 11.0};"
                "Server=ERPSQLSRV;"
                "Database=Viewpoint;"
                "Trusted_Connection=yes;"
            )
            
    if connection_type == 'PROD':
        return prod_conn_str
    elif connection_type == 'DEV':
        return dev_conn_str
    else: return

def Send_UKG_File_To_SQL():
    conn = pyodbc.connect(__return_connection_string())
    conn.timeout = 1800
    TIMEOUT = 180000
    crs = conn.cursor()

    sql_text = "exec uspEmployeeAPIImport"

    crs.execute(sql_text)
    slept = 0
    while crs.nextset():
        if slept >= TIMEOUT:
            break
        time.sleep(1)
        slept += 1

    conn.commit()
    conn.close()

