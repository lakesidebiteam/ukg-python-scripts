import os
from dotenv import load_dotenv

class env:
    def __init__(self) -> None:
        load_dotenv("Env\\dotenv.env")
        self._environment = os.environ.get('ENVIRONMENT')
        self._api_key = os.environ.get('API_KEY')
        self._ukg_user = os.environ.get('UKG_USER')
        self._password = os.environ.get('PASSWORD')
        self._short_name = os.environ.get('SHORT_NAME')
        self._infile_name = os.environ.get('INFILE_NAME')
        self._outfile_name = os.environ.get('OUTFILE_NAME')
        self._dev_file_path = os.environ.get('DEV_FILE_PATH')
        self._prod_file_path = os.environ.get('PROD_FILE_PATH')
        self._azure_storage_connection_string = os.environ.get('AZURE_STORAGE_CONNECTION_STRING')
        self._ukg_export_file = os.environ.get('UKG_EXPORT_FILE')
        self._ukg_container = os.environ.get('UKG_CONTAINER')
        self._days = os.environ.get('DAYS')
        self._token = ''

    @property
    def environment(self):
        return self._environment

    @property
    def api_key(self):
        return self._api_key

    @property
    def ukg_user(self):
        return self._ukg_user

    @property
    def password(self):
        return self._password

    @property
    def short_name(self):
        return self._short_name

    @property
    def infile_name(self):
        return self._infile_name

    @property
    def outfile_name(self):
        return self._outfile_name

    @property
    def dev_file_path(self):
        return self._dev_file_path

    @property
    def dev_working_directory(self):
        return self._dev_working_directory

    @property
    def prod_file_path(self):
        return self._prod_file_path

    @property
    def azure_storage_connection_string(self):
        return self._azure_storage_connection_string

    @property
    def ukg_export_file(self):
        return self._ukg_export_file

    @property
    def ukg_container(self):
        return self._ukg_container

    @property
    def prod_working_directory(self):
        return self._prod_working_directory

    @property
    def days(self):
        return self._days

    @property
    def token(self):
        return self._token
    
    @token.setter
    def token(self,val):
        self._token = val