import json
import requests
import pandas as pd
from datetime import datetime, timedelta
from Repo.env_var import env

__api_ChangedEmployees = 'https://middleware3.saashr.com/ta/rest/v2/companies/!{}/employees/changed'
__api_login = 'https://secure.saashr.com/ta/rest/v1/login'

e = env()
def __login_api():
    my_headers = {'Content-Type': 'application/json', 'api-key': e.api_key}
    payload = {"credentials": {
        "username": e.ukg_user,
        "password": e.password,
        "company": e.short_name
    }
    }
    response = requests.post(__api_login,
                             headers=my_headers,
                             json=payload)

    json_data = response.json() if response and response.status_code == 200 else print(
        "the status code for logging in was not 200")

    if json_data and 'token' in json_data:
        e.token = json_data.get('token')

def __call_api(api_url, par=None):
    my_headers = {'Authentication': 'Bearer ' + e.token, 'Content-Type': 'application/json'}
    request_string = api_url.format(e.short_name)
    if par != None:
        response = requests.get(request_string, headers=my_headers, params=par)
    else:
        response = requests.get(request_string, headers=my_headers)
    
    if response and response.status_code == 200:
        json_data = json.loads(response.content.decode('utf-8')) 
        return json_data
    else:
        raise Exception('Invalid Response -> Status Code: {}'.format(response.status_code))

def __LoadEmployeesWithChangesByType(KeyType):
    keys, values, index, = ['employee_id'], [], []
    date_limit = datetime.now() - timedelta(days=int(e.days))

    par = {
        "since": date_limit.isoformat()
    }
    jd = __call_api(__api_ChangedEmployees, par)

    for i in range(0, len(jd['entries'])):
        if(jd['entries'][i]['status'] != 'DELETED'):
            values.append(jd['entries'][i]['object'][KeyType])

    return values

def ReturnEmployeeIDsWithChanges():
    __login_api()
    return __LoadEmployeesWithChangesByType('employee_id')

def ReturnEmployeeAccountIDs():
    __login_api()
    return __LoadEmployeesWithChangesByType('id')
    
