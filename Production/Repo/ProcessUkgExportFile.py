import pandas as pd
import phonenumbers as pn
import os
from Repo.env_var import env
from datetime import datetime

e = env()
#Global Constants
__infile_name = e.infile_name
__outfile_name = e.outfile_name
__process_input_file_options = ['Rename', 'Delete', 'Ignore']
__process_input_file_selection = __process_input_file_options[2]

infile = __infile_name
outfile = __outfile_name

def ProcessUkgExport(employee_list):
    #Structs
    phone_fields = ['Phone', 'WorkPhone', 'CellPhone', 'ContactPhone', 'AltContactPhone']
    zip_codes = [97003, 97006, 97008, 97024, 97027, 97030, 97034, 97035, 97036, 97005, 97077, 97201, 97202, 97203, 97024, 97205, 97206, 97209, 97068, 97211, 97212, 97213, 97214, 97215, 
    97216, 97217, 97218, 97210, 97220, 97221, 97222, 97223, 97225, 97227, 97229, 97230, 97219, 97232, 97233, 97236, 97239, 97256, 97258, 97266, 97267, 97007, 97009, 97015, 97224, 97231, 
    97022, 97023, 97045, 97019, 97062, 97070, 97078, 97060, 97089, 97113, 97080, 97086, 97124, 97140, 97116, 97123]
    wa_divisions = ['000', '010', '020', '030', '031', '032', '033', '034', '035', '040', '041', '050', '051', '060', '070', '080', '090', '100', '120', '140', '150', 'PLT', 'QC', 'SHP']
    id_divisions = ['005', 'IDA', '130']
    or_divisions = ['110', '111', '112']

    #Load CSV to DataFrame
    df = pd.read_csv(infile, delimiter="|", header=0, converters={'udLocation' :str, 'Phone' :str, 'udSupervisor' :str, 'Zip' :str, 'HandicapDesc' :str, 'HandicapYN' :str, 
    'WOLocalCode' :str, 'udLocation' :str, 'WOTaxState' :str, 'HandicapYN' :str, 'StdUnempState' :str, 'StdInsState' :str, 'State' :str, 'AFServiceMedalVetYN' :str, 
    'StdInsCode' :str, 'PositionCode' :str})
    df = df[df['LastName'].str.upper() != 'TEST']
    df = df[df['LastName'].str.upper() != 'UKG ACCOUNT']
    df = df[df['LastName'].str.upper() != 'ACCOUNT']
    df = df[df['LastName'].str.upper() != 'API']
    df = df[df['HRRef'] != 13341]
    df = df[df['HRRef'].isin([int(i) for i in employee_list])]

    # Standardize Phone Numbers
    try:
        for i in df.index:

            for pf in phone_fields:
                pNum = str(df.at[i,pf])
                if pn.is_possible_number_string(pNum, 'US'):
                    df.at[i,pf] = pn.format_number(pn.parse(pNum, 'US'), pn.PhoneNumberFormat.INTERNATIONAL)[3:]

            # WOLocalCode - check to see if the Zip code falls within the Tri-met tax zone
            zip = df.at[i,'Zip']
            if str(zip) in str(zip_codes):
                df.at[i, 'WOLocalCode'] = 'Tri-Met'
            # Update WOTaxState with division state
            if df.at[i,'HRRef'] in (5070, 5071, 5072, 5073, 5074, 5094, 5142):
                df.at[i,'StdUnempState'] = 'EX'
                df.at[i,'WOTaxState'] = 'WA'
                df.at[i,'StdInsState'] = 'WA'
            elif df.at[i,'udLocation'] in wa_divisions:
                df.at[i,'WOTaxState'] = 'WA'
                df.at[i,'StdUnempState'] = 'WA'
                df.at[i,'StdInsState'] = 'WA'
            elif df.at[i,'udLocation'] in id_divisions:
                df.at[i,'WOTaxState'] = 'ID'
                df.at[i,'StdUnempState'] = 'ID'
                df.at[i,'StdInsState'] = 'ID'
            elif df.at[i,'udLocation'] in or_divisions:
                df.at[i,'WOTaxState'] = 'OR'
                df.at[i,'StdUnempState'] = 'OR'
                df.at[i,'StdInsState'] = 'OR'
            else:
                df.at[i,'WOTaxState'] = 'WA'
                df.at[i,'StdUnempState'] = 'WA'
                df.at[i,'StdInsState'] = 'WA'

            if df.at[i,'State'] in 'OR':
                df.at[i,'WOTaxState'] = 'OR'

            # StdInsCode - Update to return characters prior to the first space
            df.at[i,'StdInsCode'] = str(df.at[i,'StdInsCode']).split(' ', 1)[0]

            # StdClass - Update to return characters prior to the first space
            df.at[i,'StdClass'] = str(df.at[i,'StdClass']).split(' ', 1)[0]

            # Update HandicapYN from 0 and 1 to N and Y
            if df.at[i,'HandicapYN'] == '1':
                df.at[i, 'HandicapYN'] = 'Y'
            else:
                df.at[i,'HandicapYN'] = 'N'

            # Update AFServiceMedalVetYN
            if df.at[i,'AFServiceMedalVetYN'] == 'Yes':
                df.at[i, 'AFServiceMedalVetYN'] = 'Y'
            else:
                df.at[i,'AFServiceMedalVetYN'] = ''
            
            # Tanner Price and Katie Peabody
            if df.at[i,'HRRef'] in (13992, 11169):
                df.at[i,'StdTaxState'] = 'WA'

            # Alaska Barge Workers
            if df.at[i,'HRRef'] in (13328, 13608):
                df.at[i,'StdInsState'] = 'AK'

<<<<<<< HEAD
            # Gray Struznik should have his Insurance State be Alaska during 
            # June, July, and August. In all other months it should be WA.
            # Per Ronda Stella 6/3/2022
=======
            # Gray Struznik should have his Insurance State be Alaska during
            # June, July, and August. In all other months it should be WA.
            # Per Ronda Stella 6/3/2022

>>>>>>> 0519c78 (UKG Azure Export updates)
            if df.at[i,'HRRef'] == 11476:
                cur = datetime.now().month
                if cur in (6, 7, 8):
                    df.at[i, 'StdInsState'] = 'AK'
                else:
                    df.at[i, 'StdInsState'] = 'WA'

    except Exception as e:
        print('Error:', df.at[i,'HRRef'], e)

    #Save Output File
    if os.path.exists(outfile):
        os.remove(outfile)

    # Rename
    if __process_input_file_options[0] == __process_input_file_selection:
        update_filename = infile[:-4] + '_{}.csv'.format(str(pd.Timestamp.now()).replace(':','-')[:16])
        os.rename(infile,update_filename)
    # Delete
    elif __process_input_file_options[1] == __process_input_file_selection:
        os.remove(infile)
    # Ignore the file in all other cases
    df = df.sort_values(by='HRRef')
    
    # Generate the file and prove all this hard work was worth it
    df.to_csv(outfile, sep="|", header=False, index=False)
