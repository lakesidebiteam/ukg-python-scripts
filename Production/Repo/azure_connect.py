import os
from datetime import datetime, timedelta
from azure.core.exceptions import ResourceExistsError
from azure.storage.blob import BlobServiceClient
from azure.storage.blob import ContainerClient

def download_file_in_container(azure_con, container, filename):
    PROC_FILE = filename
    blob_service_client = BlobServiceClient.from_connection_string(azure_con)
    container_client = blob_service_client.get_container_client(container)

    blob_client = container_client.get_blob_client(PROC_FILE)

    with open('./Files/' + PROC_FILE, "wb") as my_blob:
        download_stream = blob_client.download_blob()
        my_blob.write(download_stream.readall())