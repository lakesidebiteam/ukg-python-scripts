import os
import time
from Repo.env_var import env

e=env()
class file_maintenance:
    def delete_old_files(days=7):
        
        fpath = ''
        if e.environment == 'PROD':
            fpath = e.prod_file_path
        else:
            fpath = e.dev_file_path

        current_time = time.time()
        for f in os.listdir(fpath):
            f= os.path.join(fpath,f)
            creation_time = os.path.getctime(f)
            if (current_time - creation_time) // (24*3600) >= days:
                os.remove(f)
