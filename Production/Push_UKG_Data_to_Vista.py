
from datetime import datetime
from sqlite3 import Time
from xmlrpc.client import DateTime
from py import process
import Repo.GetEmployees as emp
import Repo.ProcessUkgExportFile as proc
import shutil, os
import Repo.db_interface as dbi
from Repo.env_var import env
from Repo.Maintenance import file_maintenance as fm
from Repo.env_var import env
import Repo.azure_connect as azc

e = env()
# Call API to get Employees with Changes -> Employee IDs
employee_ids= []
employee_ids=emp.ReturnEmployeeIDsWithChanges()

#Download UKG Export File from Azure Blob Storage
azc.download_file_in_container(
        azure_con = e.azure_storage_connection_string, 
        container = e.ukg_container, 
        filename = e.ukg_export_file
        )
        
#Process UKG Export Excel File and Filter to include Employee IDs with Changes
proc.ProcessUkgExport(employee_ids)

# Move the Excel file into position so SQL Server can pick it up
my_path = os.getcwd()
file_src = os.path.join(my_path,e.outfile_name)
if e.environment == 'PROD':
    dest_path = e.prod_file_path
else:
    dest_path = e.dev_file_path
file_dest = os.path.join(dest_path,e.outfile_name)
shutil.copy(file_src, file_dest)

#Finally call the stored procedure to update the Viewpoint tables
if e.environment == 'PROD':
    dbi.Send_UKG_File_To_SQL()
    
#Call Cleanup Methods to remove old files.
# fm.delete_old_files(7)

# print('Process Complete')
